const express = require('express');
const Joi = require('joi');
const db = require('../db/db.js');
const bcrypt = require('bcrypt');
require('dotenv').config();
const jwt = require('jsonwebtoken');




const saltRounds = 10;

const schema = Joi.object().keys({
    username: Joi.string().trim().regex(/(^[a-zA-Z0-9_]*$)/).min(3).max(30).required(),
    password: Joi.string().trim().min(6).required()
});


const usersTable = db.get('users');
usersTable.createIndex('username', { unique: true });


const router = express.Router();

router.get('/', (resq, resp) => {

    resp.json({
        message: "You are on router !"
    });
})

router.post('/signup', (request, response, next) => {
    console.log(request.body);
    const result = Joi.validate(request.body, schema);
    if (result.error === null) {

        //test if the username is already used
        usersTable.findOne({ username: request.body.username })
            .then(user => {
                if (user) {
                    response.status(409);
                    response.json({
                        message: 'The user already exist'
                    })
                } else {

                    //usersTable.insert(request.body)
                    //we are gonna to hash the password
                    bcrypt.hash(request.body.password, saltRounds).then(function (hash) {
                        var data = {
                            username: request.body.username,
                            password: hash
                        };
                        usersTable.insert(data)
                            .then(function (tuple) {
                                delete data.password
                                response.json({
                                    message: 'I got you!',
                                    insertion: data
                                });
                            });
                    });
                }
            })
    } else {
        response.status(406);
        response.json({
            message: 'Incorrect format'
        })
    }
});

router.post('/signin', (request, response, next) => {

    console.log(request.body);
    const result = Joi.validate(request.body, schema);
    if (result.error === null) {
        usersTable.findOne({ username: request.body.username })
            .then((user) => {
                if (user) {
                    //check if passwords are similar
                    bcrypt.compare(request.body.password, user.password)
                        .then((res) => {
                            console.log(res);
                            if (res) {

                                //Generation of token
                                const payload = {
                                    id: user._id,
                                    username: user.username
                                };
                                const expireTime = {
                                    expiresIn: '1d'
                                };
                                console.log(process.env.TOKEN_SECRET);

                                jwt.sign(payload, process.env.TOKEN_SECRET, expireTime, (err, token) => {
                                    console.log(err)
                                    if (err) {
                                        response.status(422);
                                        response.json({
                                            message: "Please try later the server is on maintenance"
                                        })
                                    } else {
                                        response.json({
                                            message: "Clear",
                                            token
                                        });
                                    }
                                });
                            } else {
                                response.status(422);
                                response.json({
                                    message: 'Username or password incorrect'
                                });
                            }
                        })

                } else {
                    response.status(422);
                    response.json({
                        message: 'Username or password incorrect'
                    });
                }
            });

    } else {
        response.status(422);
        response.json({
            message: 'Username or password incorrect'
        });
    }

});

module.exports = router;
