
const express = require('express');
const volleyball = require('volleyball');
const auth = require('./auth/index.js');
const cors = require('cors')




app = express();
app.use(express.json({limit : "1mb"}));
app.use(volleyball);
app.use(cors({
    origin : 'http://localhost:8080'
}));

app.use('/auth',auth);
 
app.get('/',(request,response)=> {
    response.json({
        message : "Saluuuuu!"
    });
});

function errorHandler(err , request ,response , next){
    request.status(res.statusCode || 500);
    response.json({
        message : err.message,
        statck : err.stack
    })
}

server = app.listen(3000,()=>{
    console.log("Server is running on port 3000.");
});
